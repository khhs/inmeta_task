import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NavComponent } from './components/nav/nav.component';
import { MainComponent } from './components/main/main.component';
import { ServicesComponent } from './components/pages/services/services.component';
import { RouterModule } from '@angular/router';
import { NewOrderComponent } from './components/pages/new-order/new-order.component';
import { PageNotFoundComponent } from './components/pages/page-not-found/page-not-found.component';

// Services
import { OrderService } from './services/order.service';

// Routing
import {ExportedRoutes} from './app.routes';


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    MainComponent,
    ServicesComponent,
    NewOrderComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    ExportedRoutes,
    HttpClientModule,
    FormsModule
  ],
  providers: [OrderService],
  bootstrap: [AppComponent]
})
export class AppModule { }
