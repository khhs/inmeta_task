import { Injectable } from '@angular/core';
import { Order } from '../model/Order';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders, HttpEvent } from '@angular/common/http';
import { HttpParamsOptions } from '@angular/common/http/src/params';


@Injectable()
export class OrderService {

  private baseUrl = 'https://inmeta-task-rest.azurewebsites.net/api/Orders';
  private _Orders: Order[];
  Orders: Observable<Order[]>;

  constructor(private http: HttpClient) { }

  getAll(): Observable<Order[]> {
    return this.http.get<Order[]>(this.baseUrl, this.getHeaders());
  }

  getOrder(orderId: Number): Observable<Order> {
    return this.http.get<Order>(this.baseUrl + '/' + orderId, this.getHeaders());
  }

  newOrder(order: Order): Observable<Order> {
    return this.http.post<Order>(this.baseUrl, order, this.getHeaders());
  }

  updateOrder(order: Order): Observable<Order> {
    return this.http.put<Order>(this.baseUrl + '/' + order.OrderId, order, this.getHeaders());
  }

  deleteOrder(orderIndex: Number): Observable<any> {
    return this.http.delete(this.baseUrl + '/' + orderIndex, this.getHeaders());
  }

  private getHeaders(): {} {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json'
      })
    };
    return httpOptions;
  }

}
