import { RouterModule, Routes } from '@angular/router';
import { NewOrderComponent } from './components/pages/new-order/new-order.component';
import { PageNotFoundComponent } from './components/pages/page-not-found/page-not-found.component';
import { ServicesComponent } from './components/pages/services/services.component';

export const routes: Routes = [
    { path: '**', component: ServicesComponent }
];

export const ExportedRoutes = RouterModule.forRoot(routes);
