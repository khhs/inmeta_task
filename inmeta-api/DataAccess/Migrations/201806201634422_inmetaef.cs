namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inmetaef : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        OrderId = c.Int(nullable: false, identity: true),
                        Surname = c.String(unicode: false),
                        Firstname = c.String(unicode: false),
                        PhoneNo = c.String(unicode: false),
                        Email = c.String(unicode: false),
                        From = c.String(unicode: false),
                        To = c.String(unicode: false),
                        Services = c.String(unicode: false),
                        Conduct = c.String(unicode: false),
                        Note = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.OrderId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Orders");
        }
    }
}
