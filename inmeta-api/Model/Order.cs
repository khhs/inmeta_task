﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Order
    {
        public int OrderId { get; set; }
        //Customer last name
        public String Surname { get; set; }
        //Customer first name
        public String Firstname { get; set; }
        //Customer phone number
        public String PhoneNo { get; set; }
        //Customer email address
        public String Email { get; set; }
        //Customer address moving from
        public String From { get; set; }
        //Customer address moving to
        public String To { get; set; }
        //Customer chosen services
        public String Services { get; set; }
        //Date and time for when to conduct services
        public String Conduct { get; set; }
        //Notes regarding the delivery of services
        public String Note { get; set; }

        public Order()
        {

        }

    }
}
