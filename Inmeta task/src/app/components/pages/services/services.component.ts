import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../../services/order.service';
import { Order } from '../../../model/Order';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {
  Orders: Order[];
  TmpOrdersSearch: Order[];
  NewOrder = new Order();
  isLoading = false;
  EditMode = false;
  ServiceOptions = [ 'Moving', 'Packing', 'Cleaning' ];
  OrderServices = [];

  constructor(private orderHandler: OrderService) { }

  ngOnInit() {
    this.getOrders();
  }

  btnDelete_Click(orderIndex: any) {
    this.isLoading = true;
    this.orderHandler.deleteOrder(this.Orders[orderIndex].OrderId)
      .subscribe(data => {
        this.Orders.splice(orderIndex, 1);
        this.isLoading = false;
      });
  }

  btnEdit_Click(orderIndex: any) {
    this.isLoading = true;
    this.EditMode = true;
    this.NewOrder = this.Orders[orderIndex];
    this.OrderServices = this.NewOrder.Services.split(', ');
  }

  btnSave_Click() {
    this.addServicesToOrder();
    this.orderHandler.updateOrder(this.NewOrder).subscribe(() => {
      this.isLoading = false;
      this.EditMode = false;
      this.NewOrder = new Order();
    });
  }

  addService_Clicked(serviceIndex) {
    if (this.OrderServices.indexOf(this.ServiceOptions[serviceIndex], 0) === -1) {
      this.OrderServices.push(this.ServiceOptions[serviceIndex]);
    } else {
      this.OrderServices.splice(this.OrderServices.indexOf(this.ServiceOptions[serviceIndex]), 1);
    }
  }

  getOrders() {
    this.orderHandler.getAll()
      .subscribe(data => {
        this.Orders = data;
        this.TmpOrdersSearch = this.Orders;
      });
  }

  btnAddNewOrder_Click() {
    this.addServicesToOrder();
    this.orderHandler.newOrder(this.NewOrder)
      .subscribe(data => {
        this.Orders.push(data);
        this.NewOrder = new Order();
      });
  }

  addServicesToOrder() {
    this.NewOrder.Services = '';
    let seperator = '';
    for (let i = 0; i < this.OrderServices.length; i++) {
      if (i > 0) {
        seperator = ', ';
      }
      this.NewOrder.Services += seperator + this.OrderServices[i];
    }
    this.OrderServices = [];
  }

  txtInputSearch_OnChange(val) {
    if (val === '') {
      this.Orders = this.TmpOrdersSearch;
    } else {
      this.Orders = [];
      this.TmpOrdersSearch.forEach(order => {
        if (order.Surname.toLowerCase().startsWith(val)) {
          this.Orders.push(order);
        }
      });
    }
  }
}
