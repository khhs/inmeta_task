export  class Order {

    public OrderId: Number;
    // Customer last name
    public Surname: String;
    // Customer first name
    public Firstname: String;
    // Customer phone number
    public PhoneNo: String;
    // Customer email address
    public Email: String;
    // Customer address moving from
    public From: String;
    // Customer address moving to
    public To: String;
    // Customer chosen services
    public Services: String;
    // Date and time for when to conduct services
    public Conduct: String;
    // Notes regarding the delivery of services
    public Note: String;

    constructor() {}

}
